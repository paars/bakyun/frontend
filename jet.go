package frontend

import (
	"path/filepath"

	"github.com/CloudyKit/jet"
	"github.com/CloudyKit/jet/loaders/httpfs"
)

var (
	set *jet.Set
)

// JetSet returns the frontend jet template set
func JetSet() *jet.Set {
	if set == nil {
		if dev && ResourceDirectory != "" {
			set = jet.NewHTMLSet(filepath.Join(ResourceDirectory, "templates"))
			set.SetDevelopmentMode(true)
		} else {
			set = jet.NewHTMLSetLoader(httpfs.NewLoader(&subdirFS{
				fs:     fs,
				subdir: "templates",
			}))
		}
	}

	return set
}
