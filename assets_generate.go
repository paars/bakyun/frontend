//+build ignore

package main

import (
	"net/http"
	"os"
	"path/filepath"

	"github.com/shurcooL/httpfs/union"

	"github.com/shurcooL/vfsgen"
)

func panicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	cwd, err := os.Getwd()
	panicIf(err)

	fs := union.New(map[string]http.FileSystem{
		"/templates": http.Dir(filepath.Join(cwd, "assets")),
		"/assets":    http.Dir(filepath.Join(cwd, "..", "..", "public", "dist")),
	})

	err = vfsgen.Generate(fs, vfsgen.Options{
		Filename:     filepath.Join(cwd, "assets_generated.go"),
		PackageName:  "frontend",
		VariableName: "fs",
	})
	panicIf(err)
}
