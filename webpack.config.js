const path = require('path')

const CopyWebpackPlugin = require('copy-webpack-plugin')
const WebpackBuildNotifierPlugin = require('webpack-build-notifier')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { HashedModuleIdsPlugin } = require('webpack')

const pkg = require('./package.json')

module.exports = argv => {
  argv.mode = argv.mode || 'production'
  const isDev = argv.mode === 'development'

  return {
    mode: argv.mode,
    context: __dirname,
    entry: {
      variables: './src/style/variables.css',
      style: './src/style/style.css'
    },
    output: {
      path: path.join(__dirname, 'assets'),
      filename: 'script/[name].js',
      publicPath: '/'
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'buble-loader',
          include: path.join(__dirname, 'src'),
          options: {
            objectAssign: 'Object.assign',
            target: {
              firefox: 52,
              chrome: 55
            }
          }
        },
        {
          test: /\.css$/,
          use: [MiniCSSExtractPlugin.loader, 'css-loader', 'postcss-loader']
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'img/[name].[ext]'
          }
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'media/[name].[ext]'
          }
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    },
    node: {
      // prevent webpack from injecting mocks to Node native modules
      // that does not make sense for the client
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty'
    },

    optimization: {
      noEmitOnErrors: true,
      namedModules: isDev,
      namedChunks: isDev,
      nodeEnv: argv.mode,
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '~',
        name: true,
        cacheGroups: {
          css: {
            test: /\.css$/,
            reuseExistingChunk: true,
            name: 'style'
          },
          vendors: {
            test: /[\\/]node_modules[\\/].*\.js/s,
            priority: -10,
            enforce: true
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    },

    plugins: [
      new WebpackBuildNotifierPlugin({
        title: pkg.name,
        suppressSuccess: false
      }),

      new FixStyleOnlyEntriesPlugin(),
      new MiniCSSExtractPlugin({
        filename: 'style/[name].css',
        chunkFilename: 'style/[id].css'
      }),

      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, 'src', 'img'),
          to: 'img'
        }
      ]),
      // FIXME: https://github.com/jantimon/favicons-webpack-plugin

      !isDev ? new HashedModuleIdsPlugin() : null,

      new BundleAnalyzerPlugin({
        defaultSizes: 'stat',
        openAnalyzer: isDev,
        generateStatsFile: isDev,
        analyzerMode: isDev ? 'server' : 'static'
      })
    ].filter(Boolean)
  }
}
