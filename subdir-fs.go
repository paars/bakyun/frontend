package frontend

import "net/http"

type subdirFS struct {
	fs     http.FileSystem
	subdir string
}

// Open prefixes the name with the configured subdir
func (sfs *subdirFS) Open(name string) (http.File, error) {
	prefixedName := sfs.subdir + "/" + name
	return sfs.fs.Open(prefixedName)
}
