//go:generate go run assets_generate.go

package frontend

import (
	"net/http"
	"path/filepath"
)

// Assets returns a httpfs only containing the public assets
func Assets() http.FileSystem {
	if dev && ResourceDirectory != "" {
		return http.Dir(filepath.Join(ResourceDirectory, "assets"))
	}
	return &subdirFS{fs: fs, subdir: "assets"}
}
